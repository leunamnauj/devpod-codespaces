#!/bin/sh
set -e

on_exit () {
	[ $? -eq 0 ] && exit
	echo 'ERROR: Feature "base" (ghcr.io/dustinblackman/devcontainer-features/base) failed to install!'
}

trap on_exit EXIT

set -a
. ../devcontainer-features.builtin.env
. ./devcontainer-features.env
set +a

echo ===========================================================================

echo 'Feature       : base'
echo 'Description   : A set of base line debian dependencies and configurations for my devcontainers.'
echo 'Id            : ghcr.io/dustinblackman/devcontainer-features/base'
echo 'Version       : 0.0.3'
echo 'Documentation : '
echo 'Options       :'
echo '    '
echo 'Environment   :'
printenv
echo ===========================================================================

chmod +x ./install.sh
./install.sh
